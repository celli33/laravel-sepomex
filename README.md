# LARAVEL SEPOMEX

This project is a simple API to obtain data from [SEPOMEX](https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/CodigoPostal_Exportar.aspx)

## Dependencies

This project runs only with php 8.1.

## Installation

Install via composer

```bash
composer install
```

Then generate `.env`

```bash
cp .env.example .env
```

Execute

```bash
php artisan key:generate
```

Optionally creates an `.env.testing` and configure it with a different DB respect from `.env` file

```bash
cp .env .env.testing
```

You can create/update the DB using command (the txt file is including in this repo but is not recommended to use it. You can download a fresh txt file from: [SEPOMEX](https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/CodigoPostal_Exportar.aspx) alse the zipcodes.db is included but not recomendend you can use `DB_DATABASE` for
setting a custom path for your DB also you can change de connection to `MYSQL`.):

```
php artisan make:db postal_codes.txt
```

## Code Standard

For code style via PHPCS:

```bash
vendor/bin/phpcs
```

For fixing PHPCS errors via PHPCBF:

```bash
vendor/bin/phpcbf
```

For general code state via phpinsights run:

```bash
php artisan insights
```

For fixing phpinsights errros

```bash
php artisan insights --fix
```

For static analysis via rector run

```bash
vendor/bin/rector process --dry-run
```

For fixing code via rector run

```bash
vendor/bin/rector process
```

For static analysis via phpstan run:

```bash
vendor/bin/phpstan
```

For run tests:

```bash
php artisan test
```

To run all tools

```bash
composer dev:build
```

For serve the app locally

```bash
php artisan serve
```


## NOTES

The Database was designed and then used in form of `Laravel Models`.

![scheme](db-scheme.png)

The names from some fields wash sanitized to match responses whith given [example](https://jobs.backbonesystems.io/api/zip-codes/01000) but is not a good practice because it affects original data. 
