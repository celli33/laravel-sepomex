<?php

declare(strict_types=1);

use App\Models\Locality;
use App\Models\Municipality;
use App\Models\SettlementType;
use App\Models\State;
use App\Models\ZipCode;
use App\Models\ZoneType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settlements', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(SettlementType::class)->constrained();
            $table->foreignIdFor(Municipality::class)->constrained();
            $table->foreignIdFor(Locality::class)->nullable()->constrained();
            $table->foreignIdFor(ZipCode::class)->constrained();
            $table->foreignIdFor(ZoneType::class)->constrained();
            $table->foreignIdFor(State::class)->constrained();
            $table->string('name');
            $table->integer('key');
            $table->unique(['state_id', 'settlement_type_id', 'municipality_id', 'zip_code_id', 'name', 'key'], 'unique_settlement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settlements');
    }
};
