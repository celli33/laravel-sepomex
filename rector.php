<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Core\ValueObject\PhpVersion;

return static function (RectorConfig $containerConfigurator): void {
    $containerConfigurator->paths([
        __DIR__ . '/app/',
        __DIR__ . '/bootstrap/',
        __DIR__ . '/config/',
        __DIR__ . '/database/',
        __DIR__ . '/public/',
        __DIR__ . '/routes/',
        __DIR__ . '/tests/',
    ]);
    $containerConfigurator->skip([
        'config/insights.php',
    ]);

    // Define what rule sets will be applied
    $containerConfigurator->phpVersion(PhpVersion::PHP_81);

    // get services (needed for register a single rule)
    // $services = $containerConfigurator->services();

    // register a single rule
    // $services->set(TypedPropertyRector::class);
};
