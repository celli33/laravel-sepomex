<?php

declare(strict_types=1);

namespace Tests\Feature\Classes;

use App\Classes\SeedRawData;
use App\Models\Raw;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SeedRawDataTest extends TestCase
{
    use RefreshDatabase;

    public function test_seed_raw_data(): void
    {
        $seeder = new SeedRawData($this->filePath('postal_codes.csv'));
        $seeder->seed();

        $this->assertSame(2, Raw::count());
    }
}
