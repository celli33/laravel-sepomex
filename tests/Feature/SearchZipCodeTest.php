<?php

declare(strict_types=1);

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class SearchZipCodeTest extends TestCase
{
    public function test_zip_pode_search(): void
    {
        $path = $this->filePath('postal_codes.csv');
        Artisan::call("make:db {$path}");

        $response = $this->getJson(route('zip-codes.index', ['01000']));

        $response->assertOk();

        $response->assertJson(json_decode($this->fileContents('example_response.json'), true));
    }
}
