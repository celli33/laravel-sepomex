<?php

declare(strict_types=1);

namespace App\Console\Commands;

// use App\Classes\SeedRawData;

use App\Classes\SeedRawData;
use App\Models\Locality;
use App\Models\Municipality;
use App\Models\Raw;
use App\Models\Settlement;
use App\Models\SettlementType;
use App\Models\State;
use App\Models\ZipCode;
use App\Models\ZoneType;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class CreatePostalCodesDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:db {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download csv file from given path txt file and updates DB.';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        /** @var string $path */
        $path = $this->argument('path');

        if (! file_exists($path)) {
            $this->error('File not found');
            return 1;
        }

        Artisan::call('migrate:fresh');
        $seeder = new SeedRawData($path);
        $seeder->seed();
        $this->seedStates();
        $this->seedMunicipalities();
        $this->seedLocalities();
        $this->seedSettlementTypes();
        $this->seedZipCodes();
        $this->seedZoneTypes();
        $this->seedSettlements();
        DB::table('raws')->delete();
        return 0;
    }

    private function seedStates(): void
    {
        $states = Raw::select('c_estado as id', 'd_estado as name', 'c_cp as code')->distinct()->get()->toArray();
        State::upsert($states, ['id']);
    }

    private function seedMunicipalities(): void
    {
        $municipalities = Raw::select('c_estado as state_id', 'c_mnpio as key', 'd_mnpio as name')->distinct()->get()->toArray();
        Municipality::upsert($municipalities, ['key', 'state_id']);
    }

    private function seedLocalities(): void
    {
        $localities = Raw::select(
            'c_cve_ciudad as code',
            'c_estado as state_id',
            'd_ciudad as name'
        )->where('c_cve_ciudad', '!=', '')
            ->distinct()->get()->toArray();
        Locality::upsert($localities, ['id']);
    }

    private function seedSettlementTypes(): void
    {
        $settlementTypes = Raw::select('d_tipo_asenta as name')->distinct()->get()->toArray();
        SettlementType::upsert($settlementTypes, ['name']);
    }

    private function seedZipCodes(): void
    {
        $rawZipCodes = DB::table('raws')->select(
            DB::raw('cast(`d_codigo` AS INTEGER) as `id`'),
            'municipalities.id as municipality_id',
            'localities.id as locality_id',
        )->distinct()
            ->join('municipalities', function ($join): void {
                $join->on('municipalities.key', '=', DB::raw('CAST(`c_mnpio` AS INTEGER)'));
                $join->on('municipalities.state_id', '=', DB::raw('CAST(`c_estado` AS INTEGER)'));
            })
            ->leftJoin('localities', function ($join): void {
                $join->on('localities.code', '=', DB::raw('CAST(`c_cve_ciudad` AS INTEGER)'));
                $join->on('localities.state_id', '=', DB::raw('CAST(`c_estado` AS INTEGER)'));
            })->get();
        $rawZipCodes->map(function ($item) {
            return ['id' => $item->id, 'municipality_id' => $item->municipality_id, 'locality_id' => $item->locality_id];
        })->chunk(10000)->each(function (Collection $chunk): void {
            ZipCode::upsert($chunk->all(), ['id']);
        });
    }

    private function seedZoneTypes(): void
    {
        $zoneTypes = Raw::select('d_zona as name')->distinct()->get()->toArray();
        ZoneType::upsert($zoneTypes, ['name']);
    }

    private function seedSettlements(): void
    {
        $rawSettlements = DB::table('raws')->select(
            DB::raw('NULL as `id`'),
            'settlement_types.id as settlement_type_id',
            'municipalities.id as municipality_id',
            'localities.id as location_id',
            'states.id as state_id',
            'zip_codes.id as zip_code_id',
            'zone_types.id as zone_type_id',
            'd_asenta as name',
            'id_asenta_cpcons as key'
        )->distinct()
            ->join('settlement_types', 'settlement_types.name', '=', 'd_tipo_asenta')
            ->join('municipalities', function ($join): void {
                $join->on('municipalities.key', '=', DB::raw('CAST(`c_mnpio` AS INTEGER)'));
                $join->on('municipalities.state_id', '=', DB::raw('CAST(`c_estado` AS INTEGER)'));
            })
            ->join('states', function ($join): void {
                $join->on('states.id', '=', DB::raw('CAST(`c_estado` AS INTEGER)'));
            })
            ->join('zip_codes', function ($join): void {
                $join->on('zip_codes.id', '=', DB::raw('CAST(`d_codigo` AS INTEGER)'));
            })
            ->join('zone_types', 'zone_types.name', '=', 'd_zona')
            ->leftJoin('localities', function ($join): void {
                $join->on('localities.code', '=', DB::raw('CAST(`c_cve_ciudad` AS INTEGER)'));
                $join->on('localities.state_id', '=', DB::raw('CAST(`c_estado` AS INTEGER)'));
            })->get();
        $rawSettlements->map(function ($item) {
            return [
                'settlement_type_id' => $item->settlement_type_id,
                'municipality_id' => $item->municipality_id,
                'locality_id' => $item->location_id,
                'state_id' => $item->state_id,
                'zip_code_id' => $item->zip_code_id,
                'zone_type_id' => $item->zone_type_id,
                'name' => $item->name,
                'key' => $item->key,
            ];
        })->chunk(1000)->each(function (Collection $chunk): void {
            Settlement::upsert($chunk->all(), ['id']);
        });
    }
}
