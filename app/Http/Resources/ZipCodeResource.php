<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property ?\App\Models\Locality $locality
 * @property \App\Models\Municipality $municipality
 * @property \Illuminate\Database\Eloquent\Collection<int, \App\Models\Settlement> $settlements
 */
class ZipCodeResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = '';
    /**
     * Transform the resource into an array.
     *
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array<string, mixed>
     */
    public function toArray(mixed $request): mixed
    {
        $locality = $this->locality;
        $municipality = $this->municipality;
        /** @var \App\Models\State $state */
        $state = $this->municipality->state;
        $settlements = $this->settlements->map(function ($item) {
            /** @var \App\Models\ZoneType $zoneType */
            $zoneType = $item->zoneType;
            /** @var \App\Models\SettlementType $settlementType */
            $settlementType = $item->settlementTYpe;
            return [
                'key' => $item->key,
                'name' => $this->removeAccents(Str::upper($item->name)),
                'zone_type' => $this->removeAccents(Str::upper($zoneType->name)),
                'settlement_type' => [
                    'name' => $settlementType->name,
                ],
            ];
        });
        return [
            'zip_code' => str_pad((string) $this->id, 5, '0', STR_PAD_LEFT),
            'locality' => $this->removeAccents(Str::upper($locality ? $locality->name : '')),
            'federal_entity' => [
                'key' => $state->id,
                'name' => $this->removeAccents(Str::upper($state->name)),
                'code' => $state->code,
            ],
            'settlements' => $settlements,
            'municipality' => [
                'key' => $municipality->key,
                'name' => $this->removeAccents(Str::upper($municipality->name)),
            ],
        ];
    }

    public function removeAccents(string $subject): string
    {
        $search = [
            'Á', 'á',
            'É', 'é',
            'Í', 'í',
            'Ó', 'ó',
            'Ú', 'ú',
        ];
        $replace = [
            'A', 'a',
            'E', 'e',
            'I', 'i',
            'O', 'o',
            'U', 'u',
        ];
        return str_replace($search, $replace, $subject);
    }
}
