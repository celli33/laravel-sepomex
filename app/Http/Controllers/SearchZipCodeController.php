<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Resources\ZipCodeResource;
use App\Models\ZipCode;

class SearchZipCodeController extends Controller
{
    public function __invoke(ZipCode $zip): ZipCodeResource
    {
        return new ZipCodeResource($zip);
    }
}
