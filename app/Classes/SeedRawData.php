<?php

declare(strict_types=1);

namespace App\Classes;

use App\Models\Raw;
use SplFileObject;

class SeedRawData
{
    public function __construct(private string $path)
    {
    }

    public function seed(): void
    {
        $source = new SplFileObject($this->path, 'r');
        $counter = 0;
        $chunks = [];
        foreach ($source as $i => $line) {
            // discard first lines
            if ($i < 2 || is_array($line) || ! $line) {
                continue;
            }
            /** @var string $iconv */
            $iconv = iconv('iso-8859-1', 'utf-8', $line);
            $row = explode('|', $iconv);
            $dCiudad = trim($row[5]);
            $cCp = trim($row[9]);
            $cCveCiudad = trim($row[14]);
            $chunks[] = [
                'd_codigo' => trim($row[0]),
                'd_asenta' => trim($row[1]),
                'd_tipo_asenta' => trim($row[2]),
                'd_mnpio' => trim($row[3]),
                'd_estado' => trim($row[4]),
                'd_ciudad' => $dCiudad !== '' ? $dCiudad : null,
                'd_cp' => trim($row[6]),
                'c_estado' => trim($row[7]),
                'c_oficina' => trim($row[8]),
                'c_cp' => $cCp !== '' ? $cCp : null,
                'c_tipo_asenta' => trim($row[10]),
                'c_mnpio' => trim($row[11]),
                'id_asenta_cpcons' => trim($row[12]),
                'd_zona' => trim($row[13]),
                'c_cve_ciudad' => $cCveCiudad !== '' ? $cCveCiudad : null,
            ];
            $counter++;
            if ($counter > 2000) {
                Raw::upsert($chunks, ['id']);
                $counter = 0;
                $chunks = [];
            }
        }
        Raw::upsert($chunks, ['id']);
    }
}
