<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Municipality extends Model
{
    use HasFactory;

    /**
     * @var array<string>
     */
    public $with = ['state'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'key',
        'state_id',
    ];

    /**
     * @return BelongsTo<State, self>
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class);
    }
}
