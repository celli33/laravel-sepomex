<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Settlement extends Model
{
    use HasFactory;

    /**
     * @var array<string>
     */
    public $with = ['zoneType', 'settlementType'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'settlement_type_id',
        'municipality_id',
        'locality_id',
        'state_id',
        'zip_code_id',
        'zone_type_id',
        'name',
        'key',
    ];

    /**
     * @return BelongsTo<ZoneType, self>
     */
    public function zoneType(): BelongsTo
    {
        return $this->belongsTo(ZoneType::class);
    }

    /**
     * @return BelongsTo<SettlementType, self>
     */
    public function settlementType(): BelongsTo
    {
        return $this->belongsTo(SettlementType::class);
    }
}
